up: docker-up
down: docker-down
restart: docker-down docker-up
init: docker-down-clear docker-build docker-up composer copy-env wait migrations passport-install perm

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-down-clear:
	docker-compose down -v --remove-orphans

docker-build:
	docker-compose build

migrations:
	docker-compose exec php-fpm php artisan migrate

composer:
	docker-compose exec php-fpm composer install

copy-env:
	docker-compose exec php-fpm php -r "file_exists('.env') || copy('.env.example', '.env');"

passport-install:
	docker-compose exec php-fpm php artisan passport:install

perm:
	sudo chgrp -R www-data app/storage app/bootstrap/cache
	sudo chmod -R ug+rwx app/storage app/bootstrap/cache

wait:
	sleep 5