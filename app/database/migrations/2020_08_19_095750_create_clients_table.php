<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->index();
            $table->string('surname')->index();
            $table->timestamps();
        });

        Schema::create('client_phones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('client_id')
                ->references('id')
                ->on('clients')
                ->onDelete('CASCADE');
            $table->string('phone')->unique();
        });

        Schema::create('client_emails', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('client_id')
                ->references('id')
                ->on('clients')
                ->onDelete('CASCADE');
            $table->string('email')->unique();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
        Schema::dropIfExists('client_phones');
        Schema::dropIfExists('client_emails');
    }
}
