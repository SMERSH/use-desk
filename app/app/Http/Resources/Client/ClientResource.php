<?php

declare(strict_types=1);

namespace App\Http\Resources\Client;

use App\Model\Client\Entity\Email;
use App\Model\Client\Entity\Phone;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property int $id
 * @property string $name
 * @property string $surname
 */
class ClientResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'surname' => $this->surname,
            'emails' => array_map(function (Email $email) {
                return [
                    'email' => $email->email,
                ];
            }, $this->emails()->getModels()),
            'phones' => array_map(function (Phone $phone) {
                return [
                    'phone' => $phone->phone,
                ];
            }, $this->phones()->getModels()),
        ];
    }
}
