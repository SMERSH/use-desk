<?php

declare(strict_types=1);

namespace App\Http\Resources\Client;

use App\Model\Client\Entity\Email;
use App\Model\Client\Entity\Phone;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property int $id
 * @property string $name
 * @property string $surname
 */
class ListClientResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'surname' => $this->surname,
            'emails' =>  $this->emails->map(function (Email $item) {
                return ['email' => $item->email];
            })->toArray(),
            'phones' =>  $this->phones->map(function (Phone $item) {
                return ['phone' => $item->phone];
            })->toArray(),
        ];
    }
}
