<?php

namespace App\Http\Controllers\Api;

use App\Model\User\UseCase\SingUp\Command;
use App\Model\User\UseCase\SingIn\SingInRequest;
use App\Model\User\UseCase\SingUp\Handler;
use App\Model\User\UseCase\SingUp\SingUpRequest;
use App\Model\User\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use \Illuminate\Http\Response;

class AuthController extends Controller
{
    public $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param SingUpRequest $request
     * @param Handler $handler
     * @return Response
     */
    public function singUp(SingUpRequest $request, Handler $handler)
    {
        $command = new Command(
            $request->post('name'),
            $request->post('email'),
            $request->post('password')
        );

        $handler->handle($command);
        $user = $this->userRepository->getByEmail($request->post('email'));
        $token = $user->createToken('Laravel Password Grant Client')->accessToken;

        return response(['token' => $token], 200);
    }

    /**
     * @param SingInRequest $request
     * @return Response
     */
    public function login(SingInRequest $request)
    {
        $user = $this->userRepository->getByEmail($request->post('email'));

        if (Hash::check($request->post('password'), $user->password)) {
            $token = $user->createToken('Laravel Password Grant Client')->accessToken;

            return response(['token' => $token], 200);
        } else {
            $response = "Password missmatch";

            return response($response, 422);
        }
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function logout (Request $request)
    {
        $token = $request->user()->token();
        $token->revoke();

        return response('You have been succesfully logged out!', 200);
    }
}
