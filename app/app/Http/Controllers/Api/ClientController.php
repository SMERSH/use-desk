<?php

namespace App\Http\Controllers\Api;

use App\Model\Client\UseCase\Search\SearchCommand;
use App\Model\Client\UseCase\Search\SearchRequest;
use App\Http\Resources\Client\ListClientResource;
use App\Model\Client\ClientRepository;
use App\Model\Client\Entity\Client;
use App\Http\Controllers\Controller;
use App\Http\Resources\Client\ClientResource;
use App\Model\Client\UseCase\Add\AddClientRequest;
use App\Model\Client\UseCase\Add\AddClientCommand;
use App\Model\Client\UseCase\Add\AddClientHandler;
use App\Model\Client\UseCase\Remove\RemoveClientCommand;
use App\Model\Client\UseCase\Remove\RemoveClientHandler;
use App\Model\Client\UseCase\Search\SearchHandler;
use App\Model\Client\UseCase\Update\UpdateClientCommand;
use App\Model\Client\UseCase\Update\UpdateClientHandler;
use App\Model\Client\UseCase\Update\UpdateClientRequest;
use Illuminate\Http\Response;

class ClientController extends Controller
{
    /**
     * @var ClientRepository
     */
    public $repository;

    public function __construct(ClientRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param SearchRequest $request
     * @param SearchHandler $handler
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(SearchRequest $request, SearchHandler $handler)
    {
        $command = new SearchCommand(
            $request->get('name'),
            $request->get('surname'),
            $request->get('email'),
            $request->get('phone'),
            $request->get('text'),
            $request->get('page', 1),
            $request->get('perPage', 20),
            $request->get('sort'),
            $request->get('direction')
        );

        $result = $handler->handle($command);

        return ListClientResource::collection($result);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AddClientRequest $request
     * @param AddClientHandler $handler
     * @return Response
     */
    public function store(AddClientRequest $request, AddClientHandler $handler)
    {
        $command = new AddClientCommand(
            $request->post('name'),
            $request->post('surname'),
            $request->post('emails'),
            $request->post('phones')
        );

        $client = $handler->handle($command);

        return (new ClientResource($client))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);;
    }

    /**
     * Display the specified resource.
     *
     * @param Client $client
     * @return ClientResource
     */
    public function show(Client $client): ClientResource
    {
        return new ClientResource($client);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateClientRequest $request
     * @param Client $client
     * @param UpdateClientHandler $handler
     * @return ClientResource
     */
    public function update(UpdateClientRequest $request, Client $client, UpdateClientHandler $handler): ClientResource
    {
        $command = UpdateClientCommand::fromClient(
            $client,
            $request->post('name'),
            $request->post('surname'),
            $request->post('emails'),
            $request->post('phones')
        );

        $handler->handle($command);

        $updatedClient = $this->repository->getId($client->id);

        return (new ClientResource($updatedClient));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Client $client
     * @param RemoveClientHandler $handler
     * @return Response
     */
    public function destroy(Client $client, RemoveClientHandler $handler)
    {
        $command = new RemoveClientCommand($client->id);
        $handler->handle($command);

        return response(null, 204);
    }
}
