<?php

declare(strict_types=1);

namespace App\Model\Client;

use App\Model\Client\Entity\Client;
use App\Model\Client\UseCase\Search\SearchCommand;
use Illuminate\Database\Query\Expression;
use Illuminate\Pagination\LengthAwarePaginator;

class ClientRepository
{
    public function add(Client $client): Client
    {
        $client->save();

        return $client;
    }

    public function update(Client $client): void
    {
        $client->update();
    }

    public function remove(Client $client)
    {
        $delete = static function ($items) {
            foreach ($items as $item) {
                $item->delete();
            }
        };

        $delete($client->emails);
        $delete($client->phones);
        /*foreach ($client->emails as $email) {
            $email->delete();
        }
        foreach ($client->phones as $phone) {
            $phone->delete();
        }*/

        $client->delete();
    }

    public function getId($id): Client
    {
        return Client::findOrFail($id);
    }

    public function all(SearchCommand $filter)
    {
        if (!\in_array($filter->sort, [null, 'clients.id', 'name', 'surname'], true)) {
            throw new \UnexpectedValueException('Cannot sort by ' . $filter->sort);
        }

        $query = Client::with('emails', 'phones')
            ->select('clients.*')
            ->leftJoin('client_emails as ce', 'clients.id', '=', 'ce.client_id')
            ->leftJoin('client_phones as cp', 'clients.id', '=', 'cp.client_id');

        if ($filter->name) {
            $query->where('name', 'like', "%$filter->name%");
        }

        if ($filter->surname) {
            $query->where('surname', 'like', "%$filter->surname%");
        }

        if ($filter->email) {
            $query->where('ce.email', 'like', "%$filter->email%");
        }

        if ($filter->phone) {
            $query->where('cp.phone', 'like', "%$filter->phone%");
        }

        if ($filter->text) {
            $query->orWhere('name', 'like', "%$filter->text%")
                ->orWhere('surname', 'like', "%$filter->text%")
                ->orWhere('ce.email', 'like', "%$filter->text%")
                ->orWhere('cp.phone', 'like', "%$filter->text%");
        }

        if (!$filter->sort) {
            $filter->sort = 'clients.id';
            $filter->direction = $filter->direction ?: 'desc';
        } else {
            $filter->direction = $filter->direction ?: 'asc';
        }

        $query->select(new Expression('count(DISTINCT clients.id) as count'));
        $count = $query->value('count');

        $query->select('clients.*')
            ->distinct()
            ->orderBy($filter->sort, $filter->direction);

        return new LengthAwarePaginator(
            $query->forPage($filter->page, $filter->perPage)->get(),
            $count,
            $filter->perPage,
            $filter->page
        );
    }
}
