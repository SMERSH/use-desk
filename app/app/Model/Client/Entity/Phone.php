<?php

declare(strict_types=1);

namespace App\Model\Client\Entity;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * @property string $phone
 */
class Phone extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [
        'phone',
    ];

    protected $table = 'client_phones';

    protected $fillable = ['phone'];

    public $timestamps = false;
}
