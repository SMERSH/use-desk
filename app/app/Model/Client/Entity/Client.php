<?php

declare(strict_types=1);

namespace App\Model\Client\Entity;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * @property int $id
 * @property string $name
 * @property string $surname
 */
class Client extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [
        'name',
        'surname',
    ];

    protected $fillable = ['name', 'surname'];

    protected $hidden = ['created_at', 'updated_at'];

    /**
     * @param string $name
     * @param string $surname
     * @return static
     */
    public static function new(string $name, string $surname): self
    {
        return new static([
            'name' => $name,
            'surname' => $surname,
        ]);
    }

    /**
     * @param string $name
     * @param string $surname
     * @return void
     */
    public function edit(string $name, string $surname): void
    {
        $this->name = $name;
        $this->surname = $surname;
    }

    public function emails()
    {
        return $this->hasMany(Email::class, 'client_id', 'id');
    }

    public function phones()
    {
        return $this->hasMany(Phone::class, 'client_id', 'id');
    }

    /**
     * @param Email[] $emails
     * @throws \Exception
     */
    public function syncEmails(array $emails)
    {
        $current = $this->emails()->getModels();
        $new = $emails;

        $compare = static function (Email $a, Email $b): int {
            return $a->email <=> $b->email;
        };

        foreach (array_udiff($current, $new, $compare) as $item) {
            $item->delete();
        }

        foreach (array_udiff($new, $current, $compare) as $item) {
            $this->emails()->save($item);
        }
    }

    /**
     * @param Phone[] $phones
     * @throws \Exception
     */
    public function syncPhones(array $phones)
    {
        $current = $this->phones()->getModels();
        $new = $phones;

        $compare = static function (Phone $a, Phone $b): int {
            return $a->phone <=> $b->phone;
        };

        foreach (array_udiff($current, $new, $compare) as $item) {
            $item->delete();
        }

        foreach (array_udiff($new, $current, $compare) as $item) {
            $this->phones()->save($item);
        }
    }
}
