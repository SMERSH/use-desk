<?php

declare(strict_types=1);

namespace App\Model\Client\UseCase\Remove;

class RemoveClientCommand
{
    /**
     * @var int
     */
    public $id;

    public function __construct(int $id)
    {
        $this->id = $id;
    }
}
