<?php

declare(strict_types=1);

namespace App\Model\Client\UseCase\Remove;

use App\Model\Client\ClientRepository;

class RemoveClientHandler
{
    private $repository;

    public function __construct(ClientRepository $repository)
    {
        $this->repository = $repository;
    }

    public function handle(RemoveClientCommand $command): void
    {
        $client = $this->repository->getId($command->id);

        $this->repository->remove($client);
    }
}
