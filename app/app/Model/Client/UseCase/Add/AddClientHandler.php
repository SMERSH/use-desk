<?php

declare(strict_types=1);

namespace App\Model\Client\UseCase\Add;

use App\Model\Client\ClientRepository;
use App\Model\Client\Entity\Client;
use App\Model\Client\Entity\Email;
use App\Model\Client\Entity\Phone;
use App\Model\Client\Factory\EmailFactory;
use App\Model\Client\Factory\PhoneFactory;

class AddClientHandler
{
    private $repository;

    public function __construct(ClientRepository $repository)
    {
        $this->repository = $repository;
    }

    public function handle(AddClientCommand $command): Client
    {
        $client = Client::new(
            $command->name,
            $command->surname
        );

        $client = $this->repository->add($client);
        $client->syncEmails(EmailFactory::generate($command->emails));
        $client->syncPhones(PhoneFactory::generate($command->phones));

        return $client;
    }
}
