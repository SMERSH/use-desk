<?php

declare(strict_types=1);

namespace App\Model\Client\UseCase\Add;

class AddClientCommand
{
    /**
     * @var string
     */
    public $name;
    /**
     * @var string
     */
    public $surname;
    /**
     * @var array
     */
    public $emails;
    /**
     * @var array
     */
    public $phones;

    public function __construct(string $name, string $surname, array $emails, array $phones)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->emails = $emails;
        $this->phones = $phones;
    }
}
