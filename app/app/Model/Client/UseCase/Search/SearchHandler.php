<?php

declare(strict_types=1);

namespace App\Model\Client\UseCase\Search;

use App\Model\Client\ClientRepository;

class SearchHandler
{
    private $repository;

    public function __construct(ClientRepository $repository)
    {
        $this->repository = $repository;
    }

    public function handle(SearchCommand $command)
    {
        return $this->repository->all($command);
    }
}
