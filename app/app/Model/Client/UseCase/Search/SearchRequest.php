<?php

declare(strict_types=1);

namespace App\Model\Client\UseCase\Search;

use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'page' => 'nullable|numeric',
            'perPage' => 'nullable|numeric|max:100',
            'name' => 'string|max:255',
            'surname' => 'string|max:255',
            'email' => 'string|max:255',
            'phone' => 'string|max:255',
            'text' => 'string|max:255',
        ];
    }
}
