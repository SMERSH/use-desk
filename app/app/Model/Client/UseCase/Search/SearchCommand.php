<?php

declare(strict_types=1);

namespace App\Model\Client\UseCase\Search;

class SearchCommand
{
    /**
     * @var string
     */
    public $name;
    /**
     * @var string
     */
    public $surname;
    /**
     * @var string
     */
    public $email;
    /**
     * @var string
     */
    public $phone;
    /**
     * @var string
     */
    public $text;
    /**
     * @var int
     */
    public $page;
    /**
     * @var int
     */
    public $perPage;
    /**
     * @var string
     */
    public $sort;
    /**
     * @var string
     */
    public $direction;

    public function __construct(
        ?string $name,
        ?string $surname,
        ?string $email,
        ?string $phone,
        ?string $text,
        int $page,
        int $perPage,
        ?string $sort,
        ?string $direction
    ) {
        $this->name = $name;
        $this->surname = $surname;
        $this->email = $email;
        $this->phone = $phone;
        $this->text = $text;
        $this->page = $page;
        $this->perPage = $perPage;
        $this->sort = $sort;
        $this->direction = $direction;
    }
}
