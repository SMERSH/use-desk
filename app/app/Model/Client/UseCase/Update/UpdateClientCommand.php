<?php

declare(strict_types=1);

namespace App\Model\Client\UseCase\Update;

use App\Model\Client\Entity\Client;
use phpDocumentor\Reflection\DocBlock\Tags\Param;

class UpdateClientCommand
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    public $name;
    /**
     * @var string
     */
    public $surname;
    /**
     * @var array
     */
    public $emails;
    /**
     * @var array
     */
    public $phones;

    public function __construct(int $id)
    {
        $this->id = $id;
    }

    public static function fromClient(
        Client $client,
        string $name,
        string $surname,
        array $emails,
        array $phones
    ): self
    {
        $command = new self($client->id);
        $command->name = $name;
        $command->surname = $surname;
        $command->emails = $emails;
        $command->phones = $phones;

        return $command;
    }

    public function getId()
    {
        return $this->id;
    }
}
