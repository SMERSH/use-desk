<?php

declare(strict_types=1);

namespace App\Model\Client\UseCase\Update;

use Illuminate\Foundation\Http\FormRequest;

class UpdateClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'emails' => 'required|array',
            'emails.*' => 'required|string|email|max:255|unique:client_emails,client_id,' .  $this->client->id,
            'phones' => 'required|array',
            'phones.*' => 'required|string|max:12|unique:client_phones,client_id,' . $this->client->id
        ];
    }

    public function prepareForValidation()
    {
        $this->emails = is_array($this->emails) ? $this->emails: [];
        $this->emails = array_unique($this->emails);

        $this->phones = is_array($this->phones) ? $this->phones: [];
        $this->phones = array_filter(array_map(static function (string $phone) {
            preg_match('/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/', $phone, $matches);
            return $matches[0] ?? null;
        }, $this->phones));
    }
}
