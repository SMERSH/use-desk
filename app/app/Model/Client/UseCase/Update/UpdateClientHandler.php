<?php

declare(strict_types=1);

namespace App\Model\Client\UseCase\Update;

use App\Model\Client\ClientRepository;
use App\Model\Client\Entity\Email;
use App\Model\Client\Entity\Phone;
use App\Model\Client\Factory\EmailFactory;
use App\Model\Client\Factory\PhoneFactory;

class UpdateClientHandler
{
    private $repository;

    public function __construct(ClientRepository $repository)
    {
        $this->repository = $repository;
    }

    public function handle(UpdateClientCommand $command): void
    {
        $client = $this->repository->getId($command->getId());

        $client->edit($command->name, $command->surname);
        $this->repository->update($client);

        $client->syncEmails(EmailFactory::generate($command->emails));
        $client->syncPhones(PhoneFactory::generate($command->phones));
    }
}
