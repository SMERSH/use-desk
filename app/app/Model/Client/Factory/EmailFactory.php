<?php

declare(strict_types=1);

namespace App\Model\Client\Factory;

use App\Model\Client\Entity\Email;

class EmailFactory
{
    public static function generate(array $emails): array
    {
        return array_map(static function (string $email) {
            return new Email(['email' => $email]);
        }, $emails);
    }
}
