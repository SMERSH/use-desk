<?php

declare(strict_types=1);

namespace App\Model\Client\Factory;

use App\Model\Client\Entity\Phone;

class PhoneFactory
{
    public static function generate(array $phones): array
    {
        return array_map(static function (string $phone) {
            return new Phone(['phone' => $phone]);
        }, $phones);
    }
}
