<?php

declare(strict_types=1);

namespace App\Model\User\UseCase\SingUp;

use App\Model\User\Entity\User;
use App\Model\User\UserRepository;

class Handler
{
    private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function handle(Command $command): void
    {
        $user = User::singUpByEmail(
            $command->name,
            $command->email,
            $command->password
        );

        $this->repository->add($user);
    }
}
