<?php

namespace App\Model\User\Entity;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Метод регистрации пользователя по email
     *
     * @param string $name
     * @param string $email
     * @param string $password
     * @return static
     */
    public static function singUpByEmail(string $name, string $email, string $password): self
    {
        return new static([
            'name' => $name,
            'email' => $email,
            'password' => bcrypt($password),
        ]);
    }
}
