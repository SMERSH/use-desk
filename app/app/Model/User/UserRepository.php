<?php

declare(strict_types=1);

namespace App\Model\User;

use App\Model\User\Entity\User;

class UserRepository
{
    public function add(User $user): void
    {
        $user->save();
    }

    public function getByEmail($email): User
    {
        return User::where(['email' => $email])->firstOrFail();
    }
}
