<?php

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ClientController;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\HomeController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::group(['as' => 'api.', 'namespace' => 'Api'],
    function () {
        Route::get('/', 'HomeController@home');
        Route::post('/sing-up', 'AuthController@singUp')->name('sing-up');
        Route::post('/login', 'AuthController@login')->name('login');

        Route::middleware('auth:api')->group(function () {
            Route::apiResource('/clients', 'ClientController');
            Route::post('/logout', 'AuthController@logout')->name('logout');
        });
    }
);



/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
